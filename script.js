const getCube = parseInt(prompt("What number? ")) ** 3
console.log(`The cube of ${Math.cbrt(getCube)} is ${getCube}`);

const address = ["258 Washington Ave", "NW", "California", 90011];
const [street, city, state, zipCode] = address;
console.log(`I live at ${street} ${city}, ${state} ${zipCode}`);

const animal = {
	name: "Lolong",
	type: "Saltwater Crocodile",
	weight: 1075,
	length: "20 ft 3 in"
};

const {name, type, weight, length} = animal;
console.log(`${name} was a ${type.toLowerCase()}. He weighed at ${weight} kgs with a measurement of ${length}.`);

const numArray = [1, 2, 3, 4, 5];
numArray.forEach((num) => {
	return console.log(num);
});

const reduceNumber = (x, y) => x + y;
console.log(numArray.reduce(reduceNumber));

class Dog {
	constructor (name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed
	};
};

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);